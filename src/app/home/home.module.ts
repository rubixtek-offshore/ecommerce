import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{SharedModule} from '../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { ProductListComponent } from './featured-product/product-list/product-list.component';
import { ProductInfoComponent } from './featured-product/product-info/product-info.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BannerComponent } from './banner/banner.component';


@NgModule({
  declarations: [
    ProductListComponent,
    ProductInfoComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
